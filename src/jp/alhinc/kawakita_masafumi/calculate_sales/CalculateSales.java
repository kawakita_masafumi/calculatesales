package jp.alhinc.kawakita_masafumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		Map<String, String> nameMap = new HashMap<>();
		Map<String, Long> saleMap = new HashMap<>();

		//フィルタを作成する
		FilenameFilter filter = new FilenameFilter() {

			public boolean accept(File file, String str) {
				//指定文字列でフィルタする
				if (str.matches("^[0-9]{8}.rcd$")) {
					return true;
				} else {
					return false;
				}
			}
		};
		BufferedReader br = null;
		try {
			//例外エラーを回避するための処理
			File file = new File(args[0], "branch.lst");
			//Fileの存在チェック
			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {

				String[] blanStr = line.split(",");
				if (blanStr[0].matches("^[0-9]{3}") && (blanStr.length == 2)) {

					nameMap.put(blanStr[0], blanStr[1]);
				} else {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
			}

			File[] files = new File(args[0]).listFiles(filter);
			//初めのファイル名
			int firstNumber = Integer.parseInt(files[0].getName().substring(0, 8));
			//検索結果の確認
			for (int i = 0; i < files.length; ++i) {
				br = new BufferedReader(new FileReader(files[i]));

				//抜け番エラー
				int fileNumber = Integer.parseInt(files[i].getName().substring(0, 8));
				if (fileNumber - i != firstNumber) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

				String rcdline;
				List<String> rcdList = new ArrayList<>();
				while ((rcdline = br.readLine()) != null) {

					rcdList.add(rcdline);

				}
				//3行以上もしくは２行より少ないエラー処理
				if ((rcdList.size() >= 3) || (rcdList.size() < 2)) {
					System.out.println(files[i].getName() + "のフォーマットが不正です");
					return;

				}
				if (!nameMap.containsKey(rcdList.get(0))) {
					System.out.println(files[i].getName() + "の支店コードが不正です");
					return;
				}
				//支店番号、支店の売り上げをMap
				Long mapInt = Long.parseLong(rcdList.get(1));
				//もしキーが同じなら加算する
				if (saleMap.keySet().equals(nameMap.keySet())) {
					mapInt = (mapInt + saleMap.get(rcdList.get(0)));
					saleMap.put(rcdList.get(0), mapInt);
				} else {
					saleMap.put(rcdList.get(0), mapInt);
				}
				//10桁越えのエラー処理
				if (10 < String.valueOf(mapInt).length()) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
			}
		} catch (IOException e) {
			//例外エラーが起きた時に実行する処理
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}

		try {
			//fileに書き込み
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File(args[0], "branch.out"))));
			for (Map.Entry<String, String> entry : nameMap.entrySet()) {
				pw.println(entry.getKey() + "," + entry.getValue() + saleMap.get(entry.getKey()));
			}
			pw.close();

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");

		}
	}

}
